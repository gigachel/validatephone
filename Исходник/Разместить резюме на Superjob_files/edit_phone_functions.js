
function paste_handler(obj,cnt_selector,value)
{setTimeout(function(){pastePhone(obj,cnt_selector,value);},10);}
function pastePhone(obj,cnt_selector,value)
{var text=typeof(value)=='undefined'?obj.value:value;text=text.replace(/ /gi,"");var length=text.length;if((length==12||length==11)&&text.match(/(\+?\d?){1}(\d{3})(\d{3})(\d{2})(\d{2})/i)!=null)
{var phone=new Array(RegExp.$3,RegExp.$4,RegExp.$5);insert_digits(phone,obj,RegExp.$2,RegExp.$1,cnt_selector);}
else if(length==10&&text.match(/(\d{3})(\d{3})(\d{2})(\d{2})/i)!=null)
{var phone=new Array(RegExp.$2,RegExp.$3,RegExp.$4);insert_digits(phone,obj,RegExp.$1,undefined,cnt_selector);}
else if(text.match(/(\+?\d+)?[\(](\d+)[\)]([\d\-]+)/i)!=null)
{var phone=RegExp.$3;insert_digits(phone,obj,RegExp.$2,RegExp.$1,cnt_selector);}
else if(!text.match(/[^0-9\-\(\)]/g))
{var phone=text;insert_digits(phone,obj,undefined,undefined,cnt_selector);}
else
{obj.value=obj.value.replace(/[^-\d]/g,'');}}
function insert_digits(phone_char,obj,city_code,country,cnt_selector)
{var phone=((typeof phone_char)=="object")?new Array(phone_char[0],phone_char[1],phone_char[2]):phone_char;$(obj).val(((typeof phone_char)=="object")?phone[0]+"-"+phone[1]+"-"+phone[2]:phone);if(typeof city_code!="undefined")
{$(obj).closest(cnt_selector).find('input').eq(1).val(city_code);}
if(typeof country!="undefined")
{var country_code=(country!=""||country!="undefined")?country.replace(/\+/,""):"";$(obj).closest(cnt_selector).find('input').eq(0).val(country_code);}}
function phone_number_keydown_handler(evt,allow_dashes)
{var element=evt.target||evt.srcElement;var reg_exp=allow_dashes?/[^\d-]/g:/[^\d]/g;if(element.value.match(reg_exp)){return;}
if(evt.ctrlKey&&returnCharCode(evt)==86){element.pasted=true;}
if(!element.timer_running)
{element.timer_running=true;setTimeout(function(){if(element.value.match(reg_exp)){element.style.color="#b33";}},1);setTimeout(function(){element.style.color="";if(!element.pasted&&element.value.match(reg_exp)){element.value=element.value.replace(reg_exp,"");}
element.pasted=false;element.timer_running=false;},300);}}
function phones_count_handler(counter_id,add_link_id,action,max_count)
{var add_link=document.getElementById(add_link_id);var counter=document.getElementById(counter_id);if(action==='add')
{counter.innerHTML=parseInt(counter.innerHTML)+1;}
else if(action==='del')
{counter.innerHTML=parseInt(counter.innerHTML)-1;}
var cur_count=parseInt(counter.innerHTML);if(cur_count>=max_count)
{add_link.style.display='none';}
else
{add_link.style.display='inline';}}