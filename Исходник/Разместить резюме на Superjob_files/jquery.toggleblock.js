
(function($){$.fn.toggleblock=function(parameters){var defaultParameters={togglePrefix:'toggleswitch_',popupPrefix:'toggleblock_',toggleBlock:function(popup){popup.toggle();},hideBlock:function(popup){popup.hide();},onShow:function(e){},onFirstShow:function(e){}};parameters=$.extend(defaultParameters,parameters);var pRegex=new RegExp('^'+parameters.popupPrefix.replace(/([\-\[\]{}()\\\/*.^$])/g,'\\$1'),'i');this.each(function(){var body=$(window.document.body),id=this.id,toggle,popup,firstShow=true;if(pRegex.test(this.id)){toggle=$('.'+this.id.replace(pRegex,parameters.togglePrefix));popup=$(this);}
if(!popup||!toggle)
return;body.bind('click',function(){defaultParameters.hideBlock(popup);}).bind('toggleblockClicked',function(e,blockId){if(id!==blockId)
defaultParameters.hideBlock(popup);}).bind('toggleswitchClicked',function(e,blockId){if(id===blockId){defaultParameters.toggleBlock(popup)
if(firstShow){parameters.onFirstShow();parameters.onShow();firstShow=false;}
else{parameters.onShow();}}else
defaultParameters.hideBlock(popup);});popup.bind('click',function(e){body.trigger('toggleblockClicked',[id]);e.stopPropagation();});toggle.bind('click',function(e){body.trigger('toggleswitchClicked',[id]);return false;});});return this;};})(jQuery);