'use strict';angular.module('angie').constant('ResumeProcessingConfig',{cookieName:'resume_processing',interval:60000,updateUrl:'/resume_processing/update',closeUrl:'/resume_processing/close',deleteUrl:'/resume_processing/delete',cookieFormName:'form_variant'}).factory('ResumeProcessingService',['$http','ResumeProcessingConfig','$q',function($http,ResumeProcessingConfig,$q){var data_array=[];var process=false;function send(data,def){var deferred=def||$q.defer();if(process){data_array.push({data:data,deferred:deferred});}
else{process=true;$http.post(ResumeProcessingConfig.updateUrl,{data:data}).then(function(){processQueue();deferred.resolve('ok');},function(){processQueue();deferred.reject('error');});}
return deferred.promise;}
function processQueue(){process=false;if(data_array.length>0){var saved_data=data_array.shift();send(saved_data.data,saved_data.deferred);}}
function deleteProcess(){return $http.get(ResumeProcessingConfig.deleteUrl);}
function closeProcess(){return $http.get(ResumeProcessingConfig.closeUrl);}
return{send:send,deleteProcess:deleteProcess,closeProcess:closeProcess}}]).provider('ResumeProcessing',['ResumeProcessingConfig',function(ResumeProcessingConfig){var enabled=false;return{getCookie:function(name){var matches=document.cookie.match(new RegExp("(?:^|; )"+name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g,'\\$1')+"=([^;]*)"));return matches?decodeURIComponent(matches[1]):undefined;},setEnabled:function(){var cookieValue=getCookie(ResumeProcessingConfig.cookieName);if(typeof cookieValue!='undefined'&&cookieValue!=0){enabled=true;}},$get:['$timeout','ResumeProcessingConfig','ResumeProcessingService','$q',function($timeout,ResumeProcessingConfig,ResumeProcessingService,$q){var timeoutError;var timeoutSend;var step1Data={},step2Data={},educData={},reqData={},experData={};var errorsData=[];var timeStart=Date.now();var block,step=0;var resumeId=0;var registerByPhoneNumber=0;function setCookies(val){document.cookie=ResumeProcessingConfig.cookieName+'='+val+'; path=/';}
function getKeysData(data){var result=[];angular.forEach(data,function(value,key){if(angular.isObject(value)){var obj={};obj[key]=getKeysData(value);result.push(obj);}
else{result.push(key)}});return result;}
function clearData(model){var data={};var val,k;for(k in model){val=model[k];if(k[0]=='$'||_.isFunction(val)){continue;}
if(_.isObject(val)){val=clearData(val);if(!_.isEmpty(val))data[k]=val;}else{if((_.isEmpty(val)&&!_.isNumber(val))||_.isNull(val)||val==0)continue;if(typeof val=='string'){data[k]=val.replace(/^[^А-яЁё\d\w]+$/gi,'');}
else{data[k]=val;}}}
return data;}
function getClearDataKeys(data){return getKeysData(clearData(data));}
function objectEquals(x,y){if(x instanceof Function){if(y instanceof Function){return x.toString()===y.toString();}
return false;}
if(x===null||x===undefined||y===null||y===undefined){return x===y;}
if(x===y||x.valueOf()===y.valueOf()){return true;}
if(x instanceof Date){return false;}
if(y instanceof Date){return false;}
if(!(x instanceof Object)){return false;}
if(!(y instanceof Object)){return false;}
var p=Object.keys(x);return Object.keys(y).every(function(i){return p.indexOf(i)!==-1;})?p.every(function(i){return objectEquals(x[i],y[i]);}):false;}
function addErrors(errors){if(enabled){var dataToSend={step:step};if(timeoutError){$timeout.cancel(timeoutError);}
if(step==0){timeoutError=$timeout(function(){addErrors(errors);},200);return false;}
timeoutError=$timeout(function(){if(step==1){saveStep1();errors=_.filter(errors,function(obj){return RegExp('ResumeFirstBlockForm.*').test(obj.name)});}
else{saveEduc();saveReq();saveExper();errors=_.filter(errors,function(obj){return!RegExp('ResumeFirstBlockForm.*').test(obj.name)});}
var errorValues=_.values(errors),temp=[];angular.forEach(errorValues,function(item){if(!RegExp('.*function.*').test(item.label)){temp.push(item.label);}});errors=temp;if(!objectEquals(errorsData,errors)){errorsData=errors;if(_.isEmpty(errorsData))
dataToSend.error=[""];else
dataToSend.error=errorsData;sent(dataToSend);}},500);}}
function getSeconds(){var timeStop=Date.now();var diffTime=timeStop-timeStart;timeStart=timeStop;return Math.round(diffTime/1000);}
function saveStep1(dataIn,forced){var force=forced||false;var dataToSend={step:1};if(enabled&&(step==1||force)){var data=dataIn||angular.element(document.querySelector('[data-ng-controller=ModelCtrl]')).scope().model;var clearedData=getClearDataKeys(data);if(!objectEquals(step1Data,clearedData)){step1Data=clearedData;if(step1Data.length==0){dataToSend.data=[null];}
else{dataToSend.data=step1Data;}}
else{dataToSend.data={};}
if(registerByPhoneNumber==1){dataToSend.registerPhone=1;registerByPhoneNumber=0;}
if(resumeId!=0){dataToSend.resumeId=resumeId;resumeId=0;}
sent(dataToSend);}}
function saveStep2(force){var deferred=$q.defer();if(enabled&&(step==2||force)){var dataToSend={step:2};var mergedData=getKeysData(_.merge(educData,reqData,experData));if(!objectEquals(step2Data,mergedData)){step2Data=mergedData;if(step2Data.length==0){dataToSend.data=[null];}
else{dataToSend.data=step2Data;}}
else{if(mergedData.length==0){dataToSend.data=[null];}
else{dataToSend.data={}}}
sent(dataToSend).then(function(){deferred.resolve('ok');},function(){deferred.reject('error');});}
else{deferred.reject('disabled');}
return deferred.promise;}
function sent(data){var deferred=$q.defer();if(enabled){data.seconds=getSeconds();ResumeProcessingService.send(data).then(function(){deferred.resolve('ok');},function(){deferred.reject('error');});}
else{deferred.reject('disabled');}
return deferred.promise;}
function saveEduc(dataIn,forced){var deferred=$q.defer();if(enabled){var data=dataIn||angular.element(document.querySelector('[data-ng-controller=educationBlockCtrl]')).scope().model;var force=forced||false;var clearedData=clearData(data);if(!objectEquals(educData,clearedData)){educData=clearedData;}
if(timeoutSend){$timeout.cancel(timeoutSend);}
timeoutSend=$timeout(function(){saveStep2(force).then(function(){deferred.resolve('ok');},function(){deferred.reject('error');});},100);}
else{deferred.reject('disabled');}
return deferred.promise;}
function saveReq(dataIn,forced){var deferred=$q.defer();if(enabled){var data=dataIn||angular.element(document.querySelector('[data-ng-controller=requirementsBlockCtrl]')).scope().model;var force=forced||false;var clearedData=clearData(data);if(!objectEquals(reqData,clearedData)){reqData=clearedData;}
if(timeoutSend){$timeout.cancel(timeoutSend);}
timeoutSend=$timeout(function(){saveStep2(force).then(function(){deferred.resolve('ok');},function(){deferred.reject('error');});},100);}
else{deferred.reject('disabled');}
return deferred.promise;}
function saveExper(dataIn,forced){var deferred=$q.defer();if(enabled){var data=dataIn||angular.element(document.querySelector('[data-ng-controller=experienceBlockCtrl]')).scope().model;var force=forced||false;var clearedData=clearData(data);if(!objectEquals(experData,clearedData)){experData=clearedData;}
if(timeoutSend){$timeout.cancel(timeoutSend);}
timeoutSend=$timeout(function(){saveStep2(force).then(function(){deferred.resolve('ok');},function(){deferred.reject('error');});},100);}
else{deferred.reject('disabled');}
return deferred.promise;}
function setSuggest(suggest){var data={};if(enabled&&step==2){data.suggest=suggest;data.seconds=getSeconds();ResumeProcessingService.send(data);}}
function getInterval(){return ResumeProcessingConfig.interval;}
function isEnabled(){return enabled;}
function setBlock(newBlock){block=newBlock;}
function setStep(newStep){step=newStep;}
function getStep(){return step;}
function setResumeId(id){resumeId=id;}
function registerByPhone(){registerByPhoneNumber=1;}
function deleteProcessing(){var deferred=$q.defer();if(enabled){ResumeProcessingService.deleteProcess().then(function(){setCookies(0);deferred.resolve('ok');},function(){deferred.reject('error');});}
else{deferred.reject('disabled');}
return deferred.promise;}
function closeProcessing(){var deferred=$q.defer();if(enabled){ResumeProcessingService.closeProcess().then(function(){setCookies(0);deferred.resolve('ok');},function(){deferred.reject('error');});}
else{deferred.reject('disabled');}
return deferred.promise;}
return{addErrors:addErrors,saveStep1:saveStep1,saveEduc:saveEduc,saveReq:saveReq,saveExper:saveExper,getInterval:getInterval,isEnabled:isEnabled,setBlock:setBlock,setStep:setStep,getStep:getStep,setResumeId:setResumeId,registerByPhone:registerByPhone,deleteProcessing:deleteProcessing,close:closeProcessing,setSuggest:setSuggest}}]}}]);