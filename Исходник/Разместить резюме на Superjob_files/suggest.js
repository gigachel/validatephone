
(function($){function suggestInstance(input,options){var $input=$(input).attr("autocomplete","off");var $wrapper=$(document.createElement("div"));var $closer=$(options.closeHTML).bind('click',function(){$wrapper.hide();return false;});var $results=$(document.createElement("ul"));var timeout=false;var prevLength=0;var prevValue=0;var cache=[];var cacheSize=0;var self=this;var input_focused=false;var req=false;var additionalData=options.additionalData;this.source=options.source;$wrapper.addClass(options.wrapperClass).appendTo('body');$results.addClass(options.resultsClass).appendTo($wrapper);$closer.appendTo($wrapper);if($.browser.msie)
window.setTimeout(setWidth,100);else
setWidth();function setWidth(){var actualWidth=options.width||$input.width()>options.minWidth&&$input.width()||options.minWidth;$wrapper.css("width",actualWidth);}
resetPosition();$(window).load(resetPosition).resize(resetPosition);if(options.inputCanFloat)
$(window).scroll(resetPosition);$input.bind('blur',inputFocusLost).bind('focus',inputFocusObtained);$wrapper.bind('mouseover',wrapperMouseOver).bind('mouseout',wrapperMouseOut);try{$wrapper.bgiframe();}catch(e){}
if($.browser.mozilla||$.browser.opera)
$input.bind('keypress',processKey);else
$input.bind('keydown',processKey);if(options.local){var localTokens=$(options.source).html().split(options.delimiter);}
function resetPosition(){var offset=$input.offset();$wrapper.css({top:(offset.top+input.offsetHeight)+options.offset_top+'px',left:offset.left+options.offset_left+'px'});}
function inputFocusObtained(){input_focused=true;}
function inputFocusLost(){input_focused=false;setTimeout(function(){if(!$wrapper.hasClass('locked'))
$wrapper.hide()},200);}
function wrapperMouseOver(){$wrapper.addClass('locked');}
function wrapperMouseOut(){$wrapper.removeClass('locked');$input.trigger('focus');}
function lookInCache(){cached=checkCache($.trim($input.val()));if(cached){if(timeout)
clearTimeout(timeout);displayItems(cached['items']);}}
function processKey(e){if(options.switchClass&&!$input.hasClass(options.switchClass))
return true;self.cancelWait();if((/38$|40$/.test(e.keyCode)&&$wrapper.is(':visible')&&e.shiftKey==false)||(/^13$|^9$/.test(e.keyCode)&&getCurrentResult())){if(e.preventDefault)
e.preventDefault();if(e.stopPropagation)
e.stopPropagation();e.cancelBubble=true;e.returnValue=false;switch(e.keyCode){case 38:prevResult();break;case 40:nextResult();break;case 9:case 13:selectCurrentResult(true);break;}}else if(e.keyCode=='27'){if(e.preventDefault)
e.preventDefault();e.returnValue=false;if($wrapper.is(':visible')){if(e.stopPropagation)
e.stopPropagation();e.cancelBubble=true;}
$wrapper.hide();}else if(e.keyCode=='13'||e.keyCode=='9'){$wrapper.hide(0);}else{if(timeout)
clearTimeout(timeout);timeout=setTimeout(suggest,options.delay);setTimeout(lookInCache,1);}}
function suggest(){var q;if(typeof(options.getCB)=='function')
{q=options.getCB.apply($input[0]);}
else
{q=$.trim($input.val());}
if(q.length>=options.minchars){cached=checkCache(q);if(cached){displayItems(cached['items']);}else if(options.local){$wrapper.hide();var items=[];for(i=0;i<localTokens.length;i++){var item=localTokens[i].split(options.countDelimiter);if(item[0]&&item[0].match(new RegExp('(?:^|[^0-9a-zA-Zа-яА-Я])'+q.replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g,"\\$1"),'ig'))){items.push(item);}}
var text=items.join(options.delimiter);items=parseTxt(text,q);displayItems(items);addToCache(q,items,self.source.length);}else{req=$.ajax({url:self.source,cache:true,dataType:'text',type:'GET',data:additionalData?$.extend(additionalData,{q:q}):{q:q},success:function(txt){req=false;$wrapper.hide();var items=parseTxt(txt,q);displayItems(items);addToCache(q,items,txt.length);},error:function(){req=false;}});}}else{$wrapper.hide(0);}}
function checkCache(q){for(var i=0;i<cache.length;i++)
if(cache[i]['q']==q){cache.unshift(cache.splice(i,1)[0]);return cache[0];}
return false;}
function addToCache(q,items,size){while(cache.length&&(cacheSize+size>options.maxCacheSize)){var cached=cache.pop();cacheSize-=cached['size'];}
cache.push({q:q,size:size,items:items});cacheSize+=size;}
function displayItems(items){if(!items||input_focused==false)
return;if(!items.length){$wrapper.hide();return;}
var html='';for(var i=0;i<items.length;i++)
html+='<li>'+items[i]+'</li>';$results.html(html);$wrapper.show();resetPosition();$results.scrollTop(0);$results.children('li').bind('mouseover',function(){$results.children('li').removeClass(options.selectClass);$(this).addClass(options.selectClass);}).bind('click',function(e){e.preventDefault();e.stopPropagation();selectCurrentResult(true);});$input.trigger('suggest.afterShowList');}
function parseTxt(txt,q){var items=[];var tokens=txt.split(options.delimiter);for(var i=0;i<tokens.length;i++){var token=$.trim(tokens[i]);var item=token.split(options.countDelimiter);if(item[0]){if(!options.noMatchIt){token='<div>'+item[0].replace(new RegExp('(^|[^0-9a-zA-Zа-яА-Я])('+q.replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g,"\\$1")+')','ig'),function(q,w,e){return w+'<span class="'+options.matchClass+'">'+e+'</span>'})+'</div>';}else{token='<div>'+item[0]+'</div>';}}
if(item[1]){token='<div class="'+options.countClass+'">'+item[1]+'</div>'+token;}
if(token)
items[items.length]=token;}
return items;}
function getCurrentResult(){if(!$wrapper.is(':visible'))
return false;var $currentResult=$results.children('li.'+options.selectClass);if(!$currentResult.length)
$currentResult=false;return $currentResult;}
function selectCurrentResult(by_user){$currentResult=getCurrentResult();if($currentResult){var newtext=$currentResult.children().not("."+options.countClass).text();if(typeof(options.setCB)=='function')
{options.setCB.apply($input[0],[newtext]);}
else
{$input.val(newtext);}
if(by_user){$wrapper.hide();if(options.invokeBlur){$input.triggerHandler("blur")
$input.triggerHandler("focus")}
if(options.submitOnSelect){var i=$input.get(0);if(i&&i.form&&typeof(i.form.submit)=='function')
i.form.submit();}
else if(options.onSelect)
options.onSelect.apply($input[0],[$currentResult]);}
else if(options.onSelect)
options.onSelect.apply($input[0],[$currentResult]);}}
function nextResult(){$currentResult=getCurrentResult();if($currentResult)
$currentResult.removeClass(options.selectClass).next().addClass(options.selectClass);else
$results.children('li:first-child').addClass(options.selectClass);adjustScroll();}
function prevResult(){$currentResult=getCurrentResult();if($currentResult)
$currentResult.removeClass(options.selectClass).prev().addClass(options.selectClass);else
$results.children('li:last-child').addClass(options.selectClass);adjustScroll();}
function adjustScroll(){$currentResult=getCurrentResult();if($currentResult){scrtop=$results.scrollTop();reshgt=$results.innerHeight();curtop=$currentResult.position().top;nexttop=curtop+$currentResult.outerHeight();if(nexttop>reshgt)
$results.scrollTop(scrtop+nexttop-reshgt);if(curtop<0)
$results.scrollTop(scrtop+curtop);if(options.submitOnSelect)
selectCurrentResult(false);}}
this.clearCache=function(){cache=[];};this.cancelWait=function(){if(req){req.abort();req=false;}};this.dropHandlers=function(){$input.unbind('blur',inputFocusLost).unbind('focus',inputFocusObtained);$wrapper.unbind('mouseover',wrapperMouseOver).unbind('mouseout',wrapperMouseOut);$(window).unbind('resize',resetPosition);if(options.inputCanFloat)
$(window).unbind('scroll',resetPosition);if($.browser.mozilla||$.browser.opera)
$input.unbind('keypress',processKey);else
$input.unbind('keydown',processKey);$closer.unbind('click');$wrapper.remove();};this.setSource=function(source){this.source=source;this.clearCache();this.cancelWait();};this.setAdditionalData=function(data){additionalData=data;};this.destroy=function(){this.clearCache();this.cancelWait();this.dropHandlers();};}
$.fn.suggest=function(source,options){if(!source)
return;options=options||{};options.source=source;options.local=options.local||false;options.delay=options.delay||100;options.closeHTML=options.closeHTML||'<a href="#">Close</a>';options.switchClass=options.switchClass||false;options.wrapperClass=options.wrapperClass||'ac_wrapper';options.resultsClass=options.resultsClass||'ac_results';options.selectClass=options.selectClass||'ac_over';options.matchClass=options.matchClass||'ac_match';options.noMatchIt=options.noMatchIt||false;options.countClass=options.countClass||'ac_count';options.countDelimiter=options.countDelimiter||'||';options.minchars=options.minchars||2;options.minWidth=options.minWidth||100;options.width=options.width||false;options.delimiter=options.delimiter||'\n';options.setCB=options.setCB||false;options.getCB=options.getCB||false;options.onSelect=options.onSelect||false;options.submitOnSelect=options.submitOnSelect||false;options.maxCacheSize=options.maxCacheSize||65536;options.invokeBlur=options.invokeBlur||true;options.inputCanFloat=options.inputCanFloat||false;options.offset_top=options.offset_top||0;options.offset_left=options.offset_left||0;options.additionalData=options.additionalData||null;this.each(function(){var o=$.data(this,'suggest');if(o)
o.destroy();$.data(this,'suggest',new suggestInstance(this,options));});return this;};$.fn.suggestSetSource=function(source){this.each(function(){var o=$.data(this,'suggest');if(o)
o.setSource(source);});return this;};$.fn.suggestSetData=function(data){this.each(function(){var o=$.data(this,'suggest');if(o)
o.setAdditionalData(data);});return this;};$.fn.unsuggest=function(){this.each(function(){var o=$.data(this,'suggest');if(o)
o.destroy();$.data(this,'suggest',null);});return this;};})(jQuery);