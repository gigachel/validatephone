
(function(window,doc,undef){var docElement=doc.documentElement,sTimeout=window.setTimeout,firstScript=doc.getElementsByTagName("script")[0],toString={}.toString,execStack=[],started=0,noop=function(){},isGecko=("MozAppearance"in docElement.style),isGeckoLTE18=isGecko&&!!doc.createRange().compareNode,insBeforeObj=isGeckoLTE18?docElement:firstScript.parentNode,isOpera=window.opera&&toString.call(window.opera)=="[object Opera]",isIE=!!doc.attachEvent&&!isOpera,strJsElem=isGecko?"object":isIE?"script":"img",strCssElem=isIE?"script":strJsElem,isArray=Array.isArray||function(obj){return toString.call(obj)=="[object Array]";},isObject=function(obj){return Object(obj)===obj;},isString=function(s){return typeof s=="string";},isFunction=function(fn){return toString.call(fn)=="[object Function]";},globalFilters=[],scriptCache={},prefixes={timeout:function(resourceObj,prefix_parts){if(prefix_parts.length){resourceObj['timeout']=prefix_parts[0];}
return resourceObj;}},handler,yepnope;function isFileReady(readyState){return(!readyState||readyState=="loaded"||readyState=="complete"||readyState=="uninitialized");}
function injectJs(src,cb,attrs,timeout,err,internal,firstScript){var script=doc.createElement("script"),done,i;timeout=timeout||yepnope['errorTimeout'];script.src=src;for(i in attrs){script.setAttribute(i,attrs[i]);}
cb=internal?executeStack:(cb||noop);script.onreadystatechange=script.onload=function(){if(!done&&isFileReady(script.readyState)){done=1;cb();script.onload=script.onreadystatechange=null;}};sTimeout(function(){if(!done){done=1;cb(1);}},timeout);err?script.onload():firstScript.parentNode.insertBefore(script,firstScript);}
function injectCss(href,cb,attrs,timeout,err,internal,firstScript){var link=doc.createElement("link"),done,i;timeout=timeout||yepnope['errorTimeout'];cb=internal?executeStack:(cb||noop);link.href=href;link.rel="stylesheet";link.type="text/css";for(i in attrs){link.setAttribute(i,attrs[i]);}
if(!err){firstScript.parentNode.insertBefore(link,firstScript);sTimeout(cb,0);}}
function executeStack(){var i=execStack.shift();started=1;if(i){if(i['t']){sTimeout(function(){(i['t']=="c"?yepnope['injectCss']:yepnope['injectJs'])(i['s'],0,i['a'],i['x'],i['e'],1,firstScript);},0);}
else{i();executeStack();}}
else{started=0;}}
function preloadFile(elem,url,type,splicePoint,dontExec,attrObj,timeout){timeout=timeout||yepnope['errorTimeout'];var preloadElem=doc.createElement(elem),done=0,firstFlag=0,stackObject={"t":type,"s":url,"e":dontExec,"a":attrObj,"x":timeout};if(scriptCache[url]===1){firstFlag=1;scriptCache[url]=[];}
function onload(first){if(!done&&isFileReady(preloadElem.readyState)){stackObject['r']=done=1;!started&&executeStack();preloadElem.onload=preloadElem.onreadystatechange=null;if(first){if(elem!="img"){sTimeout(function(){insBeforeObj.removeChild(preloadElem)},50);}
for(var i in scriptCache[url]){if(scriptCache[url].hasOwnProperty(i)&&scriptCache[url][i].onload){scriptCache[url][i].onload();}}}}}
if(elem=="object"){preloadElem.data=url;preloadElem.setAttribute("type","text/css");}else{preloadElem.src=url;preloadElem.type=elem;}
preloadElem.width=preloadElem.height="0";preloadElem.onerror=preloadElem.onload=preloadElem.onreadystatechange=function(){onload.call(this,firstFlag);};execStack.splice(splicePoint,0,stackObject);if(elem!="img"){if(firstFlag||scriptCache[url]===2){insBeforeObj.insertBefore(preloadElem,isGeckoLTE18?null:firstScript);sTimeout(onload,timeout);}
else{scriptCache[url].push(preloadElem);}}}
function load(resource,type,dontExec,attrObj,timeout){started=0;type=type||"j";if(isString(resource)){preloadFile(type=="c"?strCssElem:strJsElem,resource,type,this['i']++,dontExec,attrObj,timeout);}else{execStack.splice(this['i']++,0,resource);execStack.length==1&&executeStack();}
return this;}
function getYepnope(){var y=yepnope;y['loader']={"load":load,"i":0};return y;}
yepnope=function(needs){var i,need,chain=this['yepnope']['loader'];function satisfyPrefixes(url){var parts=url.split("!"),gLen=globalFilters.length,origUrl=parts.pop(),pLen=parts.length,res={"url":origUrl,"origUrl":origUrl,"prefixes":parts},mFunc,j,prefix_parts;for(j=0;j<pLen;j++){prefix_parts=parts[j].split('=');mFunc=prefixes[prefix_parts.shift()];if(mFunc){res=mFunc(res,prefix_parts);}}
for(j=0;j<gLen;j++){res=globalFilters[j](res);}
return res;}
function getExtension(url){var b=url.split('?')[0];return b.substr(b.lastIndexOf('.')+1);}
function loadScriptOrStyle(input,callback,chain,index,testResult){var resource=satisfyPrefixes(input),autoCallback=resource['autoCallback'],extension=getExtension(resource['url']);if(resource['bypass']){return;}
if(callback){callback=isFunction(callback)?callback:callback[input]||callback[index]||callback[(input.split("/").pop().split("?")[0])];}
if(resource['instead']){return resource['instead'](input,callback,chain,index,testResult);}
else{if(scriptCache[resource['url']]){resource['noexec']=true;}
else{scriptCache[resource['url']]=1;}
chain.load(resource['url'],((resource['forceCSS']||(!resource['forceJS']&&"css"==getExtension(resource['url']))))?"c":undef,resource['noexec'],resource['attrs'],resource['timeout']);if(isFunction(callback)||isFunction(autoCallback)){chain['load'](function(){getYepnope();callback&&callback(resource['origUrl'],testResult,index);autoCallback&&autoCallback(resource['origUrl'],testResult,index);scriptCache[resource['url']]=2;});}}}
function loadFromTestObject(testObject,chain){var testResult=!!testObject['test'],group=testResult?testObject['yep']:testObject['nope'],always=testObject['load']||testObject['both'],callback=testObject['callback']||noop,cbRef=callback,complete=testObject['complete']||noop,needGroupSize,callbackKey;function handleGroup(needGroup,moreToCome){if(!needGroup){if(!moreToCome)complete();}
else if(isString(needGroup)){if(!moreToCome){callback=function(){var args=[].slice.call(arguments);cbRef.apply(this,args);complete();};}
loadScriptOrStyle(needGroup,callback,chain,0,testResult);}
else if(isObject(needGroup)){needGroupSize=(function(){var count=0,i
for(i in needGroup){if(needGroup.hasOwnProperty(i)){count++;}}
return count;})();for(callbackKey in needGroup){if(needGroup.hasOwnProperty(callbackKey)){if(!moreToCome&&!(--needGroupSize)){if(!isFunction(callback)){callback[callbackKey]=(function(innerCb){return function(){var args=[].slice.call(arguments);innerCb&&innerCb.apply(this,args);complete();};})(cbRef[callbackKey]);}
else{callback=function(){var args=[].slice.call(arguments);cbRef.apply(this,args);complete();};}}
loadScriptOrStyle(needGroup[callbackKey],callback,chain,callbackKey,testResult);}}}}
handleGroup(group,!!always);always&&handleGroup(always);}
if(isString(needs)){loadScriptOrStyle(needs,0,chain,0);}
else if(isArray(needs)){for(i=0;i<needs.length;i++){need=needs[i];if(isString(need)){loadScriptOrStyle(need,0,chain,0);}
else if(isArray(need)){yepnope(need);}
else if(isObject(need)){loadFromTestObject(need,chain);}}}
else if(isObject(needs)){loadFromTestObject(needs,chain);}};yepnope['addPrefix']=function(prefix,callback){prefixes[prefix]=callback;};yepnope['addFilter']=function(filter){globalFilters.push(filter);};yepnope['errorTimeout']=1e4;if(doc.readyState==null&&doc.addEventListener){doc.readyState="loading";doc.addEventListener("DOMContentLoaded",handler=function(){doc.removeEventListener("DOMContentLoaded",handler,0);doc.readyState="complete";},0);}
window['yepnope']=getYepnope();window['yepnope']['executeStack']=executeStack;window['yepnope']['injectJs']=injectJs;window['yepnope']['injectCss']=injectCss;})(this,document);(function(yepnope){var _hasOwnProperty=({}).hasOwnProperty,hasOwnProperty;if(typeof _hasOwnProperty!=='undefined'&&typeof _hasOwnProperty.call!=='undefined'){hasOwnProperty=function(object,property){return _hasOwnProperty.call(object,property);};}
else{hasOwnProperty=function(object,property){return((property in object)&&typeof object.constructor.prototype[property]==='undefined');};}
var ie=(function(){var undef,v=3,div=document.createElement('div'),all=div.getElementsByTagName('i');while(div.innerHTML='<!--[if gt IE '+(++v)+']><i></i><![endif]-->',all[0]);return v>4?v:undef;}()),iePrefixes={ie:!!ie,ie5:(ie===5),ie6:(ie===6),ie7:(ie===7),ie8:(ie===8),ie9:(ie===9),iegt5:(ie>5),iegt6:(ie>6),iegt7:(ie>7),iegt8:(ie>8),ielt7:(ie<7),ielt8:(ie<8),ielt9:(ie<9)},checkAllIEPrefixes=function(resource){var prefixes=resource.prefixes,pfx,k;for(k=0;k<prefixes.length;k++){pfx=prefixes[k];if(hasOwnProperty(iePrefixes,pfx)){if(iePrefixes[pfx]){return true;}}}
return false;},i;for(i in iePrefixes){if(hasOwnProperty(iePrefixes,i)){yepnope.addPrefix(i,function(resource){if(!checkAllIEPrefixes(resource)){resource.bypass=true;}
return resource;});}}})(this.yepnope);(function(window,doc,undef){var oldGecko=false;if("MozAppearance"in doc.documentElement.style)
oldGecko=!("onload"in doc.createElement("link"));if(oldGecko)
return;yepnope.injectCss=function(href,cb,attrs,timeout,err,internal,firstScript){var link=document.createElement("link"),onload=function(){if(!done){done=1;link.removeAttribute("id");setTimeout(cb,0);}},id="yn"+(+new Date),done,i;cb=internal?yepnope.executeStack:(cb||function(){});timeout=timeout||yepnope.errorTimeout;link.href=href;link.rel="stylesheet";link.type="text/css";link.id=id;for(i in attrs){link.setAttribute(i,attrs[i]);}
if(!err){firstScript.parentNode.insertBefore(link,firstScript);link.onload=onload;function poll(){try{var sheets=document.styleSheets;for(var j=0,k=sheets.length;j<k;j++){if(sheets[j].ownerNode.id==id){if(sheets[j].cssRules.length)
return onload();}}
if(!done)
throw new Error;}catch(e){setTimeout(poll,20);}}
poll();}else{onload();}}})(this,this.document);(function(){var pagereadyhandlers=[];var loaded={};function doASAP(callback){if(typeof($)==='function')
$(document).ready(callback);else
pagereadyhandlers.push(callback);}
function afterLoad(justLoaded,local,complete){for(var i=0;i<justLoaded.length;i++){loaded[justLoaded[i]]=true;}
local&&$('body').append(local);complete&&complete.call();}
function loadResources(resources,callback){var
remote=resources.remote,local=resources.local,remoteCount=remote.length,shouldBeLoaded=[];if(remoteCount){for(var i=0;i<remoteCount;i++){if(!loaded[remote[i]]){shouldBeLoaded.push(remote[i]);}}
if(shouldBeLoaded.length){yepnope({load:shouldBeLoaded,complete:_.partial(afterLoad,shouldBeLoaded,local,callback)});}}
if(!remoteCount||!shouldBeLoaded.length){afterLoad([],local,callback);}}
function initPage(){function initBody(){window.SJ.initRegion($('body'));}
function onPageReady(callback){function pollJQuery(){if(typeof(jQuery)==='undefined'){window.setTimeout(pollJQuery,1);}else{jQuery(document).ready(callback);}}
pollJQuery();}
var
resources={remote:[],local:''},head=document.getElementsByTagName('head')[0];if(head.onclick)
resources=head.onclick();if(resources.remote.length){yepnope({load:resources.remote,complete:function(){afterLoad(resources.remote,resources.local,function(){$.each(pagereadyhandlers,function(i,callback){$(document).ready(callback)});pagereadyhandlers=null;$(document).ready(initBody);});}});}else{onPageReady(function(){$('script').each(function(){var s=this;if(s.src){loaded[s.src]=true;}});$('link[rel="stylesheet"]').each(function(){var s=this;if(s.href){loaded[s.href]=true;}});$(document).ready(initBody);});}}
window['SJ']={};window['SJ']['doASAP']=doASAP;window['SJ']['initPage']=initPage;window['SJ']['loadResources']=loadResources;})();SJ.initPage();