
function initMMap(id_town,width,height,simple,IMG_PATH){id_town=(id_town)?id_town:4;window.metro_id_town=id_town;if(width&&height){$('#mmap').css({'background':'url('+IMG_PATH+'/img/metro/'+id_town+'.gif?p=6) no-repeat'}).width(width).height(height);window.metro_map_width=width;window.metro_map_height=height;if(typeof simple=='undefined'||!simple){width=(width)?width+250:950;height=(height)?height+0:750;}
else{window.metro_simple=true;width=(width)?width+30:950;height=(height)?height+0:750;}
$('#map_block').width(width).height(height);window.metro_without_map=false;}
else{window.metro_without_map=true;}
if(typeof MetroMap=='undefined'&&!$id('metroscript')){var
headID=document.getElementsByTagName('head')[0],script=document.createElement('SCRIPT');script.src='/js/metro.js?rcnd='+Math.round(Math.random()*10000);script.id='metroscript';script.type='text/javascript';headID.appendChild(script);}
else{MetroMap.getInstance().init();}}
function showMap(id_town,width,height,simple,IMG_PATH){window.time_to_show_metro=true;initMMap(id_town,width,height,simple,IMG_PATH);$('#container_map_block').modal({opacity:40,overlayCss:{backgroundColor:"#000"},containerId:'metro-simplemodal-container',zIndex:10015,persist:true,minWidth:100,onOpen:function(dialog){dialog.overlay.fadeIn('slow',function(){dialog.container.show();dialog.data.show();});}});return false;}
function findPos(id,elw,elh,no_handlers){__id=((typeof id=='string')?id:'txt_block');var
elem=$id(__id),d_height,d_width;try{if(document.body.clientHeight){d_height=document.body.clientHeight;d_width=document.body.clientWidth;}
else{d_height=document.body.offsetHeight;d_width=document.body.offsetWidth;}}catch(e){}
if(!elw&&!elh){elw=800;elh=550;}
show('overgray');gray=$id('overgray');gray.style.top=document.body.scrollTop+"px";show(elem);document.body.className='gray_body';document.body.style.overflow="hidden";if(d_height<elh){elem.style.height=d_height+'px';d_height=document.body.scrollTop;}
else{d_height=((d_height-elh)/2)+document.body.scrollTop;elem.style.height=elh+'px';}
d_width=(d_width-elw)/2;elem.style.display="block";elem.style.top=d_height+"px";elem.style.left=d_width+"px";if(!no_handlers){window.onscroll=function(){findPos(id,elw,elh);};window.onresize=function(){findPos(id,elw,elh);};}}
function sendMetroRequest(e,metro_box){metro_box=metro_box||'metro_box';$('#metro').hide();if(typeof window.load_metro!=='object'){window.load_metro={};}
var tid=(e.tagName=='SELECT')?e.options[e.selectedIndex].value:parseInt(e);if(window.towns_with_metro&&typeof window.towns_with_metro[tid]=='undefined'){if(window.load_metro.request){window.load_metro.request.abort();}
fillMetro('',metro_box);return false;}
var ajaxSettings={};ajaxSettings.global=false;ajaxSettings.cache=true;ajaxSettings.ifModified=true;$('#loadin').show();$.ajaxSetup(ajaxSettings);if(window.load_metro.request){window.load_metro.request.abort();}
window.load_metro.request=$.get('/js/request/metro.php?town='+tid,function(text){try{text=eval('('+text+')');}
catch(e){return;}
$('#selected-town-id').val(text.tid);fillMetro(text,metro_box);});}
function fillMetro(metro_data,id)
{$('#loadin').hide();if(typeof MetroMap!=='undefined'){MetroMap.destroy();}
var metro_root=$('#'+id);if(typeof metro_data!='undefined'&&metro_data!='')
{metro_root.show();metro_root.data('metro_data',metro_data);if(typeof metro_data!=='object'){try{metro_data=eval('('+metro_data+')');}catch(e){throw new Error("eval('('+"+metro_data+"+')'); failed: "+e.message);}}
if(metro_data.coordinates){window.coordinates=eval('('+metro_data.coordinates+')');window.time_to_show_metro=false;}
if(metro_data.towns_with_metro)
{window.towns_with_metro=eval('('+metro_data.towns_with_metro+')');}
if(metro_data.selected_stations){$('#doodeedee',metro_root).html(metro_data.selected_stations);$('#clear_metro_list').show();}
else
{$('#doodeedee',metro_root).empty();$('#clear_metro_list').hide();}
if(typeof metro_data.hasMap!='undefined'){if(metro_data.hasMap==false){$('#watch_map',metro_root).hide();}
else{$('#watch_map',metro_root).show().unbind('click').click(function(){showMap(metro_data.tid,metro_data.map_width,metro_data.map_height,false,metro_data.IMG_PATH);});}}
initMMap(metro_data.tid,metro_data.map_width,metro_data.map_height,false,metro_data.IMG_PATH);}
else{metro_root.data('metro_data','');$('#doodeedee',metro_root).empty();metro_root.hide();}}
function getIEVersionNumber(){var ua=navigator.userAgent;var offcet=ua.indexOf("MSIE ");if(offcet==-1){return false;}
else{var num=parseFloat(ua.substring(offcet+5,ua.indexOf(";",offcet)));return(num<7);}}
TWindow=function(){this.is_ie=getIEVersionNumber();};TWindow.prototype.unload=function(){this.unloaded=true;window.__id=null;window.onscroll=null;window.onresize=null;hide(this.id);hide('overgray');document.body.className='';document.body.style.overflow="auto";if(this.is_ie){els=document.getElementsByTagName('SELECT');for(i=0;i<=els.length;i++){show(els[i]);}}
return false;};TWindow.prototype.init=function(id,elw,elh){this.unloaded=false;this.id=id;this.el=$id(this.id);if(!$id('closeit')){if(typeof window.metro_simple!='undefined'){this.el.style.paddingTop='15px';this.el.style.zIndex='150';}
ell=this.el;ell.innerHTML+='<div class="closeit"><a id="closeit" onClick="return TWindow.getInstance().unload();" href="#"><b>Закрыть окно [X]</b></a></div>';}
$id(this.id).style.width=elw+'px';$id(this.id).style.height=elh+'px';findPos(this.id,elw,elh);findPos(this.id,elw,elh);show(this.id);if(this.is_ie){els=document.getElementsByTagName('SELECT');for(i=0;i<=els.length;i++){hide(els[i]);}}};TWindow.getInstance=function(){if(!TWindow.instance){TWindow.instance=new TWindow();}
return TWindow.instance;};